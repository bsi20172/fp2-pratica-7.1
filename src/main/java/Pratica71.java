
import java.util.ArrayList;
import static java.util.Collections.binarySearch;
import static java.util.Collections.sort;
import java.util.List;
import java.util.Scanner;
import utfpr.ct.dainf.if62c.pratica.Jogador;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * Template de projeto de programa Java usando Maven.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica71 {
    public static void main(String[] args) {
        System.out.println("Digite o número de jogadores: ");
        Scanner scanner1 = new Scanner(System.in);
        int tam = scanner1.nextInt();
        int numero;
        int indice;
        Jogador novo;
        List<Jogador> time = new ArrayList<>();
        String nome;
        for(int i = 0;i < tam;i++){
            System.out.println("Digite o número da camisa do jogador: ");
            scanner1 = new Scanner(System.in);
            if(scanner1.hasNextInt()){
                numero = scanner1.nextInt();
                System.out.println("Digite o nome do jogador: ");
                scanner1 = new Scanner(System.in);
                nome = scanner1.next();
                time.add(new Jogador(numero,nome));
            }
        }
        sort(time);
        for(Jogador aux:time){
            System.out.println(aux.toString());   
        }
        numero = 1;
        while(numero != 0){
            System.out.println("Digite o número da camisa do jogador: ");
            scanner1 = new Scanner(System.in);
            
            if(scanner1.hasNextInt()){
                numero = scanner1.nextInt();
                if(numero != 0){
                    System.out.println("Digite o nome do jogador: ");
                    scanner1 = new Scanner(System.in);
                    nome = scanner1.next();
                    novo = new Jogador(numero,nome);
                    if(binarySearch(time,novo) >= 0){
                        indice = binarySearch(time,novo);
                        time.set(indice,novo);
                    }
                    else{
                        indice = (binarySearch(time,novo) + 1)* -1;
                        time.add(indice,novo);
                    }
                    for(Jogador aux:time){
                        System.out.println(aux.toString());   
                    }
                }
            }
        }
    }
}
